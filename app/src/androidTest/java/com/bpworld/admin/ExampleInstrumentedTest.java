package com.bpworld.admin;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.bpworld.admin.model.OauthToken;

import org.junit.Test;
import org.junit.runner.RunWith;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();


        App.getApi().getOauthToken("2","k6WTx6IuUT9OAe3YfZAHWn2TRJyio27CuIiL0Cze","cashier","password", "cashiers").enqueue(new Callback<OauthToken>() {
            @Override
            public void onResponse(Call<OauthToken> call, Response<OauthToken> response) {
                assertEquals(response.body().getTokenType(),"Bearer");
            }

            @Override
            public void onFailure(Call<OauthToken> call, Throwable t) {

            }
        });
    }
}
