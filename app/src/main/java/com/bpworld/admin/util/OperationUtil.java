package com.bpworld.admin.util;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

/**
 * Created by GrinfeldRA
 */

public class OperationUtil {

    public static void setBackgroundDrawable(StateListDrawable stateListDrawable, String color){
        DrawableContainer.DrawableContainerState drawableContainerState = (DrawableContainer.DrawableContainerState) stateListDrawable.getConstantState();
        Drawable[] children = drawableContainerState.getChildren();
        GradientDrawable drawable = (GradientDrawable) children[0];
        drawable.setColor(Color.parseColor(color));
    }

}
