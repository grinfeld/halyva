package com.bpworld.admin;

import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;

/**
 * Created by GrinfeldRA
 */

public class DatabaseContentProvider extends ContentProvider {

    @Override
    protected Configuration getConfiguration() {
        Configuration.Builder builder = new Configuration.Builder(getContext());
        builder.addModelClass(DataCompanies.class);
        builder.addModelClass(InnerColor.class);
        return builder.create();
    }
}