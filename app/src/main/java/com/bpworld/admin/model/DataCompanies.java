package com.bpworld.admin.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

@Table(name = "DataComp",id = "_id")
public class DataCompanies extends Model {
    @Column()
    @SerializedName("id")
    @Expose
    private Integer id_companies;
    @Column()
    @SerializedName("inner_color")
    @Expose
    private InnerColor innerColor;
    @Column()
    @SerializedName("inner_name")
    @Expose
    private String innerName;

    public DataCompanies() {
        super();
    }

    public Integer getIdCompanies() {
        return id_companies;
    }

    public void setIdCompanies(Integer id) {
        this.id_companies = id;
    }

    public InnerColor getInnerColor() {
        return innerColor;
    }

    public void setInnerColor(InnerColor innerColor) {
        this.innerColor = innerColor;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

}
