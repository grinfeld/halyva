package com.bpworld.admin.model;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;

/**
 * Created by GrinfeldRA on 18.06.2018.
 */

public class OperationItem {

    private int id;
    private String amount;
    private String discount;
    private Date date;
    private String imagePath;
    private List<OperationItem> operationItems;

    public OperationItem(int id, @NonNull String amount, @NonNull String discount, @NonNull Date date, @NonNull String imagePath) {
        this.id = id;
        this.amount = amount;
        this.discount = discount;
        this.date = date;
        this.imagePath = imagePath;
    }

    public OperationItem(@NonNull String amount, @NonNull String discount){
        this.amount = amount;
        this.discount = discount;
    }

    public OperationItem(List<OperationItem> operationItems) {
        this.operationItems = operationItems;
    }

    public String getAmount() {
        return amount;
    }

    public String getDiscount() {
        return discount;
    }

    public Date getDate() {
        return date;
    }

    public List<OperationItem> getOperationItems() {
        return operationItems;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getId() {
        return id;
    }
}
