package com.bpworld.admin.model;

/**
 * Created by GrinfeldRA
 */

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cashiers implements Serializable{

    @SerializedName("finish")
    @Expose
    private Boolean finish;
    @SerializedName("object")
    @Expose
    private List<Object> object = null;

    public Boolean getFinish() {
        return finish;
    }

    public void setFinish(Boolean finish) {
        this.finish = finish;
    }

    public List<Object> getObject() {
        return object;
    }

    public void setObject(List<Object> object) {
        this.object = object;
    }


    public class Company implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("inner_color_id")
        @Expose
        private Integer innerColorId;
        @SerializedName("inner_name")
        @Expose
        private String innerName;
        @SerializedName("inner_color")
        @Expose
        private InnerColor innerColor;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getInnerColorId() {
            return innerColorId;
        }

        public void setInnerColorId(Integer innerColorId) {
            this.innerColorId = innerColorId;
        }

        public String getInnerName() {
            return innerName;
        }

        public void setInnerName(String innerName) {
            this.innerName = innerName;
        }

        public InnerColor getInnerColor() {
            return innerColor;
        }

        public void setInnerColor(InnerColor innerColor) {
            this.innerColor = innerColor;
        }

    }


    public class InnerColor implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("hex")
        @Expose
        private String hex;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }

    }


    public class Object implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("fio")
        @Expose
        private String fio;
        @SerializedName("login")
        @Expose
        private String login;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("is_active")
        @Expose
        private Boolean isActive;
        @SerializedName("total_discount_count")
        @Expose
        private Integer totalDiscountCount;
        @SerializedName("total_trade_turnover")
        @Expose
        private Integer totalTradeTurnover;
        @SerializedName("today_discount_count")
        @Expose
        private Integer todayDiscountCount;
        @SerializedName("today_trade_turnover")
        @Expose
        private Integer todayTradeTurnover;
        @SerializedName("company")
        @Expose
        private Company company;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getFio() {
            return fio;
        }

        public void setFio(String fio) {
            this.fio = fio;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        public Integer getTotalDiscountCount() {
            return totalDiscountCount;
        }

        public void setTotalDiscountCount(Integer totalDiscountCount) {
            this.totalDiscountCount = totalDiscountCount;
        }

        public Integer getTotalTradeTurnover() {
            return totalTradeTurnover;
        }

        public void setTotalTradeTurnover(Integer totalTradeTurnover) {
            this.totalTradeTurnover = totalTradeTurnover;
        }

        public Integer getTodayDiscountCount() {
            return todayDiscountCount;
        }

        public void setTodayDiscountCount(Integer todayDiscountCount) {
            this.todayDiscountCount = todayDiscountCount;
        }

        public Integer getTodayTradeTurnover() {
            return todayTradeTurnover;
        }

        public void setTodayTradeTurnover(Integer todayTradeTurnover) {
            this.todayTradeTurnover = todayTradeTurnover;
        }

        public Company getCompany() {
            return company;
        }

        public void setCompany(Company company) {
            this.company = company;
        }

    }
}