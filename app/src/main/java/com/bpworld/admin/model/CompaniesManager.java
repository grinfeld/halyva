package com.bpworld.admin.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompaniesManager {

    @SerializedName("finish")
    @Expose
    private Boolean finish;
    @SerializedName("object")
    @Expose
    private List<DataCompanies> object = null;

    public Boolean getFinish() {
        return finish;
    }

    public void setFinish(Boolean finish) {
        this.finish = finish;
    }

    public List<DataCompanies> getData() {
        return object;
    }

    public void setData(List<DataCompanies> object) {
        this.object = object;
    }

}