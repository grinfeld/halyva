package com.bpworld.admin.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by GrinfeldRA
 */
@Table(name = "InnerColor",id = "_id")
public class InnerColor extends Model {

    public InnerColor() {
        super();
    }
    @Column()
    @SerializedName("hex")
    @Expose
    private String hex;
    @Column(name = "DataCompanies")
    public DataCompanies dataCompanies;

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }
}
