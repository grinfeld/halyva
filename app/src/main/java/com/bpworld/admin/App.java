package com.bpworld.admin;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.bpworld.admin.interfaces.Api;
import com.bpworld.admin.util.*;
import com.bpworld.admin.util.TLSSocketFactory;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSocketFactory;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bpworld.admin.util.PicassoUtil.provideX509TrustManager;

/**
 * Created by GrinfeldRA
 */

public class App extends com.activeandroid.app.Application {

    private static Api api;
    public final static String KEY_SECRET = "k6WTx6IuUT9OAe3YfZAHWn2TRJyio27CuIiL0Cze";
    private static final String APP_PREFERENCES = "mysettings";
    private static final String TOKEN = "token";
    private static final String COMPANY = "company";
    private static final String COMPANY2 = "company2";
    private static SharedPreferences mSettings;
    private static String BEARER = "Bearer ";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        OkHttpClient client=new OkHttpClient();
        try {
            client = new OkHttpClient.Builder()
                    .addInterceptor(new ConnectivityInterceptor(this))
                    .sslSocketFactory(new TLSSocketFactory()).build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.str_url_api))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(Api.class);
    }


    public static Api getApi() {
        return api;
    }

    public static String getCompany2(){
        String company = mSettings.getString(COMPANY2, null);
        if (company!=null){
            return company;
        }
        return null;
    }

    public static void setCompany2(String company){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(COMPANY2, company);
        editor.apply();
    }



    public static String getCompany(){
        String company = mSettings.getString(COMPANY, null);
        if (company!=null){
            return company;
        }
        return null;
    }

    public static void setCompany(String company){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(COMPANY, company);
        editor.apply();
    }

    public static void setToken(String token){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public static String getToken(){
        String token  = mSettings.getString(TOKEN, null);
        if (token!=null){
            return BEARER+token;
        }
        return null;
    }

}
