package com.bpworld.admin.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.Cashiers;
import com.bpworld.admin.util.PicassoUtil;
import com.zcw.togglebutton.ToggleButton;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterExpand extends RecyclerView.Adapter<RecyclerViewAdapterExpand.ViewHolder> {

    private static final String TAG = "#RecyclerExpand";
    private LayoutInflater mInflater;
    private Cashiers data;
    private ItemClickListenerRecyclerView mClickListener;
    private int mExpandedPosition = -1;
    private Context context;

    public RecyclerViewAdapterExpand(Context context, Cashiers data, ItemClickListenerRecyclerView mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.data = data;
        this.mClickListener = mClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.recyclerview_row_expand, parent, false);
        return new ViewHolder(rootView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Cashiers.Object object = data.getObject().get(position);
        holder.txtName.setText(object.getFio());
        if (object.getIsActive()){
            holder.toggle_btn.setToggleOn();
        }else {
            holder.toggle_btn.setToggleOff();
        }
        holder.txt_today_total_discount.setText(object.getTodayDiscountCount()+" скидок на сумму "+object.getTodayTradeTurnover()+" р.");
        PicassoUtil.preparePicasso(context).load(object.getAvatar()).into(holder.profile_image);
        final boolean isExpanded = position == mExpandedPosition;
        holder.view_details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.action_arrow.setImageResource(isExpanded ? R.mipmap.arrow_top : R.mipmap.arrow_down);
        holder.itemView.setActivated(isExpanded);
        holder.action_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClickRecyclerView(v, position, isExpanded);
                    mExpandedPosition = isExpanded ? -1 : position;
                    notifyDataSetChanged();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClickRecyclerView(v, position, isExpanded);
            }
        });
        holder.toggle_btn.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                int myInt = on ? 1 : 0;
                App.getApi().cashierStatus(App.getToken(),object.getId(),myInt).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d(TAG, response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
    }

    public Cashiers.Object getItem(int position){
        return data.getObject().get(position);
    }

    @Override
    public int getItemCount() {
        return data.getObject().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public static final int TAG_ARROW = 0;
        public static final int TAG_ITEM = 1;
        TextView txtName,txt_today_total_discount;
        ImageView action_arrow;
        View view_details;
        CircleImageView profile_image;
        ToggleButton toggle_btn;

        ViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            action_arrow = itemView.findViewById(R.id.action_arrow);
            view_details = itemView.findViewById(R.id.view_details);
            profile_image = itemView.findViewById(R.id.profile_image);
            txt_today_total_discount = itemView.findViewById(R.id.txt_today_total_discount);
            toggle_btn = itemView.findViewById(R.id.toggle_btn);
            action_arrow.setTag(TAG_ARROW);
            itemView.setTag(TAG_ITEM);
        }
    }

    public interface ItemClickListenerRecyclerView {
        void onItemClickRecyclerView(View view, int position, boolean isExpanded);
    }

}
