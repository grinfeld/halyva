package com.bpworld.admin.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpworld.admin.R;
import com.bpworld.admin.model.OperationSummary;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterOperationSummary extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<ListItem> items;
    private Activity context;
    private LayoutInflater mInflater;

    public RecyclerViewAdapterOperationSummary(List<ListItem> items, Activity context) {
        this.items = items;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case ListItem.TYPE_DISCOUNT:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_discount, parent, false);
                return new DiscountViewHolder(itemView);
            }
            case ListItem.TYPE_SUMMARY:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_summary, parent, false);
                return new SummaryViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_DISCOUNT: {
                DiscountItem discountItem = (DiscountItem) items.get(position);
                DiscountViewHolder viewHolder = (DiscountViewHolder) holder;
                viewHolder.txt_discount.setText(discountItem.getOperationItem().getDiscount());
                viewHolder.txt_amount.setText(discountItem.getOperationItem().getAmount());
                break;
            }
            case ListItem.TYPE_SUMMARY: {
                SummaryItem summaryItem = (SummaryItem) items.get(position);
                SummaryViewHolder viewHolder = (SummaryViewHolder) holder;
                viewHolder.txt_discount_point.setText(summaryItem.getDiscount_point());
                viewHolder.txt_amount_point.setText(summaryItem.getAmount_point());
                viewHolder.txt_discount.setText(summaryItem.getDiscount());
                viewHolder.txt_total_amount.setText(summaryItem.getTotal_amount());
                viewHolder.txt_original_amount.setText(summaryItem.getOriginal_amount());
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public static abstract class ListItem {

        static final int TYPE_DISCOUNT = 0;
        static final int TYPE_SUMMARY = 1;
        abstract public int getType();
    }

    public static class SummaryItem extends ListItem {

        @NonNull
        private String amount_point, discount_point,total_amount, discount, original_amount;

        public SummaryItem(@NonNull String amount_point, @NonNull String discount_point, @NonNull String total_amount, @NonNull String discount, @NonNull String original_amount) {
            this.amount_point = amount_point;
            this.discount_point = discount_point;
            this.total_amount = total_amount;
            this.discount = discount;
            this.original_amount = original_amount;
        }

        @NonNull
        public String getAmount_point() {
            return amount_point;
        }

        @NonNull
        public String getDiscount_point() {
            return discount_point;
        }

        @NonNull
        public String getTotal_amount() {
            return total_amount;
        }

        @NonNull
        public String getDiscount() {
            return discount;
        }

        @NonNull
        public String getOriginal_amount() {
            return original_amount;
        }

        @Override
        public int getType() {
            return TYPE_SUMMARY;
        }

    }

    public static class DiscountItem extends ListItem {

        @NonNull
        private OperationSummary operationItem;

        public DiscountItem(@NonNull OperationSummary operationItem) {
            this.operationItem = operationItem;
        }

        @NonNull
        public OperationSummary getOperationItem() {
            return operationItem;
        }

        @Override
        public int getType() {
            return TYPE_DISCOUNT;
        }

    }

    private class DiscountViewHolder extends RecyclerView.ViewHolder {

        TextView txt_amount, txt_discount;

        public DiscountViewHolder(View itemView) {
            super(itemView);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_discount = itemView.findViewById(R.id.txt_discount);
        }
    }

    private class SummaryViewHolder extends RecyclerView.ViewHolder {

        TextView txt_amount_point, txt_discount_point,txt_total_amount, txt_discount, txt_original_amount;

        public SummaryViewHolder(View itemView) {
            super(itemView);
            txt_discount = itemView.findViewById(R.id.txt_discount);
            txt_amount_point = itemView.findViewById(R.id.txt_amount_point);
            txt_discount_point = itemView.findViewById(R.id.txt_discount_point);
            txt_total_amount = itemView.findViewById(R.id.txt_total_amount);
            txt_original_amount = itemView.findViewById(R.id.txt_original_amount);

        }
    }
}
