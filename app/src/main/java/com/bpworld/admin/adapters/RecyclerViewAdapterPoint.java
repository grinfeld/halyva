package com.bpworld.admin.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.CompaniesManager;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;
import com.bpworld.admin.util.OperationUtil;

import java.util.List;

/**
 * Created by GrinfeldRA.
 */

public class RecyclerViewAdapterPoint extends  RecyclerView.Adapter<RecyclerViewAdapterPoint.ViewHolder> {


    private LayoutInflater mInflater;
    private Activity context;
    private List<InnerColor> innerColors;
    private  List<DataCompanies> dataCompanies;
    private String key;

    public RecyclerViewAdapterPoint(Activity context, String key) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        innerColors = new Select().from(InnerColor.class).execute();
        dataCompanies = new Select().from(DataCompanies.class).execute();
        this.key = key;
    }

    @NonNull
    @Override
    public RecyclerViewAdapterPoint.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.recyclerview_row_point, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txt_title.setText(dataCompanies.get(position).getInnerName());
        StateListDrawable stateListDrawable = (StateListDrawable) holder.view.getBackground();
        OperationUtil.setBackgroundDrawable(stateListDrawable, innerColors.get(position).getHex());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (key==null){
                    App.setCompany(dataCompanies.get(position).getIdCompanies().toString());
                }else {
                    App.setCompany2(dataCompanies.get(position).getIdCompanies().toString());
                }
                context.onBackPressed();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dataCompanies.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            view = itemView.findViewById(R.id.circle);
        }
    }
}
