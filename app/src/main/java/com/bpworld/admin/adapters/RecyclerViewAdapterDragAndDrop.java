package com.bpworld.admin.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpworld.admin.R;
import com.bpworld.admin.interfaces.ItemTouchHelperCallback;
import com.bpworld.admin.interfaces.OnStartDragListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterDragAndDrop extends RecyclerView.Adapter<RecyclerViewAdapterDragAndDrop.BindsItemViewHolder> implements ItemTouchHelperCallback {

    private final OnStartDragListener mDragStartListener;

    private List<String> mItems = new ArrayList<>();

    public RecyclerViewAdapterDragAndDrop(OnStartDragListener mDragStartListener, List<String> mItems) {
        this.mDragStartListener = mDragStartListener;
        this.mItems = mItems;

    }

    @NonNull
    @Override
    public BindsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_drag_and_drop, parent, false);
        return new BindsItemViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final BindsItemViewHolder holder, int position) {
        holder.textView.setText(mItems.get(position));
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mItems, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }


    static class BindsItemViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        BindsItemViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txtName);
        }
    }
}

