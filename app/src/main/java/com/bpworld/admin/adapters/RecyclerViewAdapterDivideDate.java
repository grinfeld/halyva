package com.bpworld.admin.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.OperationItem;
import com.bpworld.admin.model.OperationSummary;
import com.bpworld.admin.model.OperationsItemCashier;
import com.bpworld.admin.task.AsyncBlur;
import com.bpworld.admin.util.DateUtils;
import com.bpworld.admin.util.OperationUtil;
import com.bpworld.admin.util.PicassoUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterDivideDate extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AsyncBlur.IOnCompleteTask{

    private static final String TAG = "#RecyclerDivideDate";
    @NonNull
    private List<ListItem> items = Collections.emptyList();
    private Activity context;
    private LayoutInflater mInflater;
    private ProgressDialog progress;

    public RecyclerViewAdapterDivideDate(@NonNull List<ListItem> items, Activity context) {
        this.items = items;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case ListItem.TYPE_HEADER:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_header, parent, false);
                return new HeaderViewHolder(itemView);
            }
            case ListItem.TYPE_BODY:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_body, parent, false);
                return new BodyViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                HeaderItem header = (HeaderItem) items.get(position);
                HeaderViewHolder viewHolder = (HeaderViewHolder) holder;
                viewHolder.txt_header.setText(DateUtils.formatDate(header.getDate()));
                break;
            }
            case ListItem.TYPE_BODY: {
                final BodyItem bodyItem = (BodyItem) items.get(position);
                BodyViewHolder viewHolder = (BodyViewHolder) holder;
                viewHolder.txt_amount.setText(bodyItem.getOperationItem().getAmount());
                viewHolder.txt_discount.setText(bodyItem.getOperationItem().getDiscount());
                PicassoUtil.preparePicasso(context).load(bodyItem.getOperationItem().getImagePath()).into(viewHolder.profileImage);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progress = new ProgressDialog(context);
                        progress.setMessage(context.getString(R.string.mes_wait_for_load));
                        progress.setCancelable(true);
                        progress.show();
                        startBitmapAsync(bodyItem.getOperationItem().getId());

                    }
                });
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    private void startBitmapAsync(int id){
        AsyncBlur asyncBlur = new AsyncBlur(context, id, this);
        asyncBlur.execute();
    }

    @Override
    public void onCompleteTask(AsyncBlur asyncBlur, int id) {
        progress.dismiss();
        try {
            Bitmap bitmap = asyncBlur.get();
            showDialog(bitmap, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog(Bitmap bitmap, int id){
        final Dialog dialog = new Dialog(context, R.style.Theme_D1NoTitleDim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_operation_summary);
        final CircleImageView profileImage = dialog.findViewById(R.id.profile_image);
        final TextView txt_fio = dialog.findViewById(R.id.txt_fio);
        final TextView txt_point = dialog.findViewById(R.id.txt_point);
        final View view = dialog.findViewById(R.id.circle);
        final Drawable draw = new BitmapDrawable(context.getResources(), bitmap);
        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        final RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        App.getApi().getOperations(App.getToken(), id).enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String jsonString = jsonObject.getString("data");
                        jsonString = "{\"data\":"+jsonString+"}";
                        Log.d(TAG, jsonString);
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                        Gson gson = builder.create();
                        final OperationsItemCashier operationsItemCashier = gson.fromJson(jsonString, OperationsItemCashier.class);
                        PicassoUtil.preparePicasso(context).load(operationsItemCashier.getData().getUser().getPersonalInformation().getAvatar()).into(profileImage);
                        txt_fio.setText(operationsItemCashier.getData().getUser().getPersonalInformation().getFirstName()+" "
                                + operationsItemCashier.getData().getUser().getPersonalInformation().getLastName());
                        txt_point.setText(operationsItemCashier.getData().getCompany().getInnerName());
                        OperationUtil.setBackgroundDrawable((StateListDrawable) view.getBackground(),operationsItemCashier.getData().getCompany().getInnerColor().getHex());
                        List<RecyclerViewAdapterOperationSummary.ListItem> items = new ArrayList<>();
                        for (OperationsItemCashier.Discount discount : operationsItemCashier.getData().getDiscountDetail().getDiscounts()){
                            OperationSummary operationSummary = new OperationSummary(discount.getTitle(),
                                    discount.getOriginalPrice().toString(),discount.getDiscountValue().toString());
                            items.add(new RecyclerViewAdapterOperationSummary.DiscountItem(operationSummary));
                        }
                        items.add(new RecyclerViewAdapterOperationSummary.SummaryItem(
                                operationsItemCashier.getData().getDiscountDetail().getDiscountPrice().toString()+"Р",
                                operationsItemCashier.getData().getDiscountDetail().getDrawPointsValue().toString()+"Б",
                                operationsItemCashier.getData().getTotalPrice().toString()+"Р",
                                operationsItemCashier.getData().getDiscountPrice().toString()+"Р",
                                operationsItemCashier.getData().getPrice().toString()+"Р"));
                        recyclerView.setAdapter(new RecyclerViewAdapterOperationSummary(items, context));
                        dialog.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public ListItem getItem(int position){
        return items.get(position);
    }




    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView txt_header;

        HeaderViewHolder(View itemView) {
            super(itemView);
            txt_header = itemView.findViewById(R.id.txt_header);
        }

    }

    private class BodyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_amount, txt_discount;
        CircleImageView profileImage;

        BodyViewHolder(final View itemView) {
            super(itemView);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_discount = itemView.findViewById(R.id.txt_discount);
            profileImage = itemView.findViewById(R.id.profile_image);
        }


    }

    public static abstract class ListItem {

        static final int TYPE_HEADER = 0;
        static final int TYPE_BODY = 1;
        abstract public int getType();
    }

    public static class HeaderItem extends ListItem {

        @NonNull
        private Date date;

        public HeaderItem(@NonNull Date date) {
            this.date = date;
        }

        @NonNull
        Date getDate() {
            return date;
        }

        @Override
        public int getType() {
            return TYPE_HEADER;
        }

    }

    public static class BodyItem extends ListItem {

        @NonNull
        private OperationItem operationItem;

        public BodyItem(@NonNull OperationItem operationItem) {
            this.operationItem = operationItem;
        }

        @NonNull
        public OperationItem getOperationItem() {
            return operationItem;
        }

        @Override
        public int getType() {
            return TYPE_BODY;
        }

    }

}
