package com.bpworld.admin.view;

import android.app.Activity;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.adapters.RecyclerViewAdapterPoint;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;
import com.bpworld.admin.util.OperationUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by GrinfeldRA.
 */

public class PointFragment extends Fragment {


    private static final String TAG = "#PointFragment";
    private Activity context;

    @BindView(R.id.recycler_point)
    RecyclerView recycler_point;
    private RecyclerViewAdapterPoint recyclerViewAdapterExpand;
    @BindView(R.id.txt_title)
    TextView point_name;
    @BindView(R.id.circle)
    View point_circle;
    private DataCompanies currentDataCompanies;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        View rootView  = inflater.inflate(R.layout.fragment_point, container, false);
        ButterKnife.bind(this, rootView);
        if (context != null) {
            ActionBar actionBar  = ((AppCompatActivity)context).getSupportActionBar();
            if (actionBar!=null){
                View view = actionBar.getCustomView();
                TextView btn_action = view.findViewById(R.id.btn_action);
                TextView txt_title = view.findViewById(R.id.txt_title);
                txt_title.setText(R.string.str_point);
                btn_action.setVisibility(View.GONE);
            }
        }
        String key = null;
        if (getArguments()!=null){
            key = getArguments().getString("KEY");
        }
        recycler_point.setLayoutManager(new LinearLayoutManager(context));
        recyclerViewAdapterExpand = new RecyclerViewAdapterPoint(context, key);
        recycler_point.setAdapter(recyclerViewAdapterExpand);
        List<InnerColor> innerColors = new Select().from(InnerColor.class).execute();
        List<DataCompanies> dataCompanies = new Select().from(DataCompanies.class).execute();
        int currentPosition = 0;
        for (DataCompanies companies : dataCompanies){
            String company;
            if (key == null){
                company = App.getCompany();
            }else {
                company = App.getCompany2();
            }
            if (company != null && company.equals(companies.getIdCompanies().toString())) {
                currentDataCompanies = companies;
                break;
            }
            currentPosition += 1;
        }
        if (currentDataCompanies!=null){
            OperationUtil.setBackgroundDrawable((StateListDrawable) point_circle.getBackground(), innerColors.get(currentPosition).getHex());
            point_name.setText(currentDataCompanies.getInnerName());
        }
        return rootView;
    }


}
