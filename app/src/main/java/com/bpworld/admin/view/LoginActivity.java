package com.bpworld.admin.view;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.OauthToken;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "#LoginActivity";

    @BindView(R.id.input_login)
    EditText emailText;
    @BindView(R.id.input_password)
    EditText passwordText;
    Handler handler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar_login, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setCustomView(viewActionBar, params);
        }
        passwordText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this,R.drawable.ic_password), null);
        emailText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this,R.drawable.ic_login), null);
        handler = new Handler(Looper.getMainLooper());
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                   login();
                }
                return false;
            }
        });
        if (App.getToken()!=null){
            startMainActivity(true);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_sign_in:
                login();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void startMainActivity(boolean b){
        startActivity(new Intent(this, MainActivity.class));
        finish();
        if (b){
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }


    public void login() {
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        if (email.isEmpty() && password.isEmpty()){
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
        }else {
            App.getApi().getOauthToken("2", App.KEY_SECRET, email, password, "managers").enqueue(new Callback<OauthToken>() {
                @Override
                public void onResponse(Call<OauthToken> call, Response<OauthToken> response) {
                    if (response.isSuccessful()){
                        OauthToken oauthToken = response.body();
                        if (oauthToken != null) {
                            App.setToken(oauthToken.getData().getAccessToken());
                            Log.d(TAG, oauthToken.getData().getAccessToken());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    startMainActivity(true);
                                }
                            });
                        }
                    }else {
                        try {
                            String strResponse = response.errorBody().string();
                            Log.d(TAG, strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            JSONObject data = jsonObject.getJSONObject("errors");
                            String description = data.getString("description");
                            Toast.makeText(LoginActivity.this, description, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<OauthToken> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }



    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {

    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Неправильный логин или пароль", Toast.LENGTH_LONG).show();

    }

    public boolean validate() {
        boolean valid = true;
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        if (email.isEmpty()) {
            emailText.setError("логин не может быть пустым");
            valid = false;
        } else {
            emailText.setError(null);
        }
        if (password.isEmpty()) {
            passwordText.setError("пароль не может быть пустым");
            valid = false;
        } else {
            passwordText.setError(null);
        }
        return valid;
    }

}
