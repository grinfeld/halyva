package com.bpworld.admin.view;

import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.adapters.RecyclerViewAdapterDivideDate;
import com.bpworld.admin.interfaces.contract.EmployeesItemFragmentContract;
import com.bpworld.admin.model.Cashiers;
import com.bpworld.admin.model.OperationItem;
import com.bpworld.admin.model.OperationsCashier;
import com.bpworld.admin.presenter.EmployeesItemFragmentPresenter;
import com.bpworld.admin.util.OperationUtil;
import com.bpworld.admin.util.PicassoUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by GrinfeldRA
 */

public class EmployeesItemFragment extends Fragment implements View.OnClickListener,EmployeesItemFragmentContract.view{

    private static final String TAG = "#EmployeesItemFragment";
    @BindView(R.id.recycler_divide_date)
    RecyclerView recycler_divide_date;
    @BindView(R.id.profile_image)
    CircleImageView circleImageView;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.circle)
    View circle_view;
    @BindView(R.id.point_name)
    TextView point_name;
    @BindView(R.id.btn_send_password)
    Button btn_send_password;
    @BindView(R.id.btn_delete_employees)
    Button btn_delete_employees;

    View rootView;
    Handler handler;
    Cashiers.Object object;

    EmployeesItemFragmentPresenter presenter;

    @NonNull
    private List<RecyclerViewAdapterDivideDate.ListItem> items = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_employees_item, container, false);
        presenter = new EmployeesItemFragmentPresenter(this);
        ButterKnife.bind(this, rootView);
        handler = new Handler(Looper.getMainLooper());
        btn_delete_employees.setOnClickListener(this);
        btn_send_password.setOnClickListener(this);
        object = (Cashiers.Object) getArguments().getSerializable("DATA");
        PicassoUtil.preparePicasso(getActivity()).load(object.getAvatar()).into(circleImageView);
        txt_name.setText(object.getFio());
        point_name.setText(object.getCompany().getInnerName());
        OperationUtil.setBackgroundDrawable((StateListDrawable) circle_view.getBackground(),object.getCompany().getInnerColor().getHex());
        App.getApi().getOperationsCashier(App.getToken(),object.getId()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String strResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(strResponse);
                    String jsonString = jsonObject.getString("data");
                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                    Gson gson = builder.create();
                    final OperationsCashier operationsCashier = gson.fromJson(jsonString, OperationsCashier.class);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Map<Date, List<OperationItem>> operations = toMap(loadOperations(operationsCashier));
                            for (Date date : operations.keySet()) {
                                RecyclerViewAdapterDivideDate.HeaderItem header = new RecyclerViewAdapterDivideDate.HeaderItem(date);
                                items.add(header);
                                for (OperationItem event : operations.get(date)) {
                                    RecyclerViewAdapterDivideDate.BodyItem item = new RecyclerViewAdapterDivideDate.BodyItem(event);
                                    items.add(item);
                                }
                            }
                            recycler_divide_date.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycler_divide_date.setAdapter(new RecyclerViewAdapterDivideDate(items, getActivity()));

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



        return rootView;
    }

    @NonNull
    private List<OperationItem> loadOperations(OperationsCashier operationsCashier) {
        List<OperationItem> operations = new ArrayList<>();
        for (OperationsCashier.Object object : operationsCashier.getObject()){
            try {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = format.parse(object.getCreatedAt());
                operations.add(new OperationItem(object.getId(),"Сумма: " +object.getTotalPrice()+"Р",
                        "Скидка: "+object.getDiscountPrice()+"Р",
                        date, object.getUser().getPersonalInformation().getAvatar()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return operations;
    }


    @NonNull
    private Map<Date, List<OperationItem>> toMap(@NonNull List<OperationItem> operations) {
        Map<Date, List<OperationItem>> map = new TreeMap<>();
        for (OperationItem event : operations) {
            List<OperationItem> value = map.get(event.getDate());
            if (value == null) {
                value = new ArrayList<>();
                map.put(event.getDate(), value);
            }
            value.add(event);
        }
        return map;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_delete_employees:
                presenter.onActionDeleteEmployees();
                break;
            case R.id.btn_send_password:
                presenter.onActionSendPassword();
                break;
        }
    }

    @Override
    public void onStartSendPassword() {
        CustomDialog customDialog = new CustomDialog();
        Bundle bundle =new Bundle();
        bundle.putSerializable(CustomDialog.TYPE_DIALOG, CustomDialog.TypeDialog.SEND_PASSWORD);
        bundle.putInt("DATA", object.getId());
        customDialog.setArguments(bundle);
        customDialog.show(getFragmentManager().beginTransaction(),CustomDialog.TypeDialog.SEND_PASSWORD.toString());
    }

    @Override
    public void onStartDeleteEmployees() {
        CustomDialog customDialog = new CustomDialog();
        Bundle bundle =new Bundle();
        bundle.putSerializable(CustomDialog.TYPE_DIALOG, CustomDialog.TypeDialog.DELETE_EMPLOYEES);
        bundle.putInt("DATA", object.getId());
        customDialog.setArguments(bundle);
        customDialog.show(getFragmentManager().beginTransaction(),CustomDialog.TypeDialog.DELETE_EMPLOYEES.toString());
    }
}
