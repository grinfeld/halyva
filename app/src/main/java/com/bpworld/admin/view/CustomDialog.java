package com.bpworld.admin.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.Cashiers;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bpworld.admin.view.CustomDialog.TypeDialog.DELETE_EMPLOYEES;
import static com.bpworld.admin.view.CustomDialog.TypeDialog.SEND_PASSWORD;

/**
 * Created by GrinfeldRA
 */

public class CustomDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "#CustomDialog";
    public static String TYPE_DIALOG = "type";
    private TypeDialog typeDialog;

    enum TypeDialog {SEND_PASSWORD, DELETE_EMPLOYEES, CREATE_USER}

    public Button yes, no;
    TextView textView;
    Handler handler;
    int id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog, container);
        handler = new Handler(Looper.getMainLooper());
        yes = view.findViewById(R.id.btn_yes);
        no = view.findViewById(R.id.btn_no);
        textView = view.findViewById(R.id.txt_dia);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        id = getArguments().getInt("DATA");
        typeDialog = (TypeDialog) getArguments().getSerializable(TYPE_DIALOG);
        if (typeDialog != null) {
            switch (typeDialog) {
                case SEND_PASSWORD:
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_green, yes);
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_red, no);
                    yes.setText("Отправить");
                    no.setText("Отмена");
                    textView.setText("Вы действительно хотите отправить пароль пользователю?");
                    break;
                case DELETE_EMPLOYEES:
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_green, yes);
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_red, no);
                    yes.setText("Удалить");
                    no.setText("Отмена");
                    textView.setText("Вы действительно хотите удалить данного пользователя?");
                    break;
            }
        }
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                switch (typeDialog) {
                    case DELETE_EMPLOYEES:
                        App.getApi().deleteCashier(App.getToken(), id).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        Toast.makeText(getActivity(), "Ок", Toast.LENGTH_SHORT).show();
                                        dismiss();
                                        getFragmentManager().popBackStack();
                                    } else {
                                        Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                        dismiss();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                    dismiss();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                dismiss();
                            }
                        });
                        break;
                    case SEND_PASSWORD:
                        App.getApi().sendPassword(App.getToken(), id).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                                    if (response.isSuccessful()) {
                                        Toast.makeText(getActivity(), "Ок", Toast.LENGTH_SHORT).show();
                                        dismiss();
                                    } else {
                                        Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                        dismiss();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                    dismiss();
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                dismiss();
                            }
                        });

                        break;
                }
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
    }

    public void setBackgroundDrawable(Context context, int id, View view) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, id));
        } else {
            view.setBackground(ContextCompat.getDrawable(context, id));
        }
    }

}
