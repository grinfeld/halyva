package com.bpworld.admin.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.adapters.RecyclerViewAdapterExpand;
import com.bpworld.admin.interfaces.OnStartDragListener;
import com.bpworld.admin.interfaces.contract.EmployeesFragmentContract;
import com.bpworld.admin.model.Cashiers;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;
import com.bpworld.admin.presenter.EmployeesFragmentPresenter;
import com.bpworld.admin.util.OperationUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA.
 */

public class EmployeesFragment extends Fragment implements RecyclerViewAdapterExpand.ItemClickListenerRecyclerView, View.OnClickListener, OnStartDragListener, EmployeesFragmentContract.view {

    public static final String TAG = "#EmployeesFragment";
    public static final int TAG_ACTION_EDIT = 1;
    public static final int TAG_ACTION_SAVE = 2;

    private RecyclerViewAdapterExpand recyclerViewAdapterExpand;

    @BindView(R.id.action_point_view)
    View action_point_view;
    @BindView(R.id.action_point)
    ImageView action_point;
    @BindView(R.id.recycler_expand)
    RecyclerView recyclerView;
    @BindView(R.id.txt_title)
    TextView point_name;
    @BindView(R.id.circle)
    View point_circle;

    private EmployeesFragmentPresenter presenter;
    private ItemTouchHelper mItemTouchHelper;
    private Context context;
    private TextView btn_action;
    private TextView txt_title;
    private Handler handler;
    private DataCompanies currentDataCompanies;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_employees,container,false);
        ButterKnife.bind(this, rootView);
        context = getActivity();
        handler = new Handler(Looper.getMainLooper());
        presenter = new EmployeesFragmentPresenter(this);
        if (context != null) {
            ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
                btn_action = viewActionBar.findViewById(R.id.btn_action);
                txt_title = viewActionBar.findViewById(R.id.txt_title);
                btn_action.setVisibility(View.VISIBLE);
                btn_action.setTag(TAG_ACTION_EDIT);
                btn_action.setOnClickListener(this);
                btn_action.setText(getString(R.string.str_edit_employees));
                txt_title.setText(getString(R.string.str_employees));
                actionBar.setCustomView(viewActionBar);
                actionBar.setElevation(0);
                actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            }
        }

        action_point.setVisibility(View.VISIBLE);
        action_point_view.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        List<InnerColor> innerColors = new Select().from(InnerColor.class).execute();
        List<DataCompanies> dataCompanies = new Select().from(DataCompanies.class).execute();
        int currentPosition = 0;
        for (DataCompanies companies : dataCompanies){
            if (App.getCompany().equals(companies.getIdCompanies().toString())){
                currentDataCompanies = companies;
                break;
            }
            currentPosition += 1;
        }
        if (currentDataCompanies!=null){
            OperationUtil.setBackgroundDrawable((StateListDrawable) point_circle.getBackground(), innerColors.get(currentPosition).getHex());
            point_name.setText(currentDataCompanies.getInnerName());
            Map<String, String> param = new LinkedHashMap<>();
            param.put("company_id", currentDataCompanies.getIdCompanies().toString());
            App.getApi().getCashiers(App.getToken(),param).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()){
                            String strResponse = response.body().string();
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String jsonString = jsonObject.getString("data");
                            GsonBuilder builder = new GsonBuilder();
                            builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                            Gson gson = builder.create();
                            Log.d(TAG, jsonString);
                            final Cashiers cashiers = gson.fromJson(jsonString, Cashiers.class);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerViewAdapterExpand = new RecyclerViewAdapterExpand(context, cashiers, EmployeesFragment.this);
                                    recyclerView.setAdapter(recyclerViewAdapterExpand);

                                }
                            });
                        }else {
                            String strResponse = response.errorBody().string();
                            Log.d(TAG, String.valueOf(strResponse));
                            //String jsonString = strResponse.get("errors");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

        return rootView;
    }

    @Override
    public void onItemClickRecyclerView(View view, int position, boolean isExpanded) {
        switch ((int)view.getTag()){
            case RecyclerViewAdapterExpand.ViewHolder.TAG_ARROW:

                break;
            case RecyclerViewAdapterExpand.ViewHolder.TAG_ITEM:
                presenter.onActionRecyclerViewItem(position);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_action:
                switch ((int)view.getTag()){
                    case TAG_ACTION_EDIT:
                        presenter.onActionEdit();
                        break;
                    case TAG_ACTION_SAVE:
                        presenter.onActionSave();
                        break;
                }
                break;
            case R.id.action_point_view:
                presenter.onActionPoint();
                break;
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onStartEdit() {
//        RecyclerViewAdapterDragAndDrop bindsRecyclerListAdapter = new RecyclerViewAdapterDragAndDrop(this, data);
//        ItemTouchHelper.Callback callback = new BindsItemTouchHelperCallback(bindsRecyclerListAdapter);
//        mItemTouchHelper = new ItemTouchHelper(callback);
//        mItemTouchHelper.attachToRecyclerView(recyclerView);
//        recyclerView.setAdapter(bindsRecyclerListAdapter);
//        btn_action.setText(getString(R.string.str_save));
//        btn_action.setTag(TAG_ACTION_SAVE);
//        action_point.setVisibility(View.GONE);
    }


    @Override
    public void onStartSave() {
        btn_action.setText(getString(R.string.str_edit_employees));
        btn_action.setTag(TAG_ACTION_EDIT);
        action_point.setVisibility(View.VISIBLE);
//        recyclerViewAdapterExpand = new RecyclerViewAdapterExpand(context, data, this);
//        recyclerView.setAdapter(recyclerViewAdapterExpand);
    }

    @Override
    public void onStartPoint() {
        getFragmentManager().beginTransaction().replace(R.id.container, new PointFragment()).addToBackStack(null).commit();
    }


    @Override
    public void onStartEmployeesItem(int position) {
        btn_action.setVisibility(View.GONE);
        txt_title.setText("");
        Cashiers.Object o = recyclerViewAdapterExpand.getItem(position);
        //Object o - set args EmployeesItemFragment
        FragmentManager fragmentManager  = getFragmentManager();
        FragmentTransaction fragmentTransaction;
        if (fragmentManager != null) {
            fragmentTransaction = fragmentManager.beginTransaction();
            EmployeesItemFragment employeesItemFragment = new EmployeesItemFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("DATA", (Serializable) o);
            employeesItemFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.container, employeesItemFragment)
                    .addToBackStack(null).commit();
        }
    }


}
