package com.bpworld.admin.view;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Delete;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.interfaces.contract.MainActivityContract;
import com.bpworld.admin.model.CompaniesManager;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;
import com.bpworld.admin.presenter.MainActivityPresenter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.json.JSONObject;

import java.lang.reflect.Modifier;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MainActivityContract.view, BottomNavigationView.OnNavigationItemSelectedListener{

    private MainActivityPresenter presenter;

    @BindView(R.id.bottom_navigation)
    BottomNavigationViewEx bottomNavigationView;
    Handler handler;
    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenter(this);
        ButterKnife.bind(this);
        context = this;
        handler =new Handler(Looper.getMainLooper());
        bottomNavigationView.setTextVisibility(false);
        //initIconSizeBottomNavigationMenuView();
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.getOnNavigationItemSelectedListener().onNavigationItemSelected(bottomNavigationView.getMenu().getItem(0));

    }

    @Override
    public void startEmployeesFragment(@NonNull FragmentTransaction fragmentTransaction) {
        initCompanies(fragmentTransaction);
    }

    @Override
    public void startNewEmployeesFragment(@NonNull FragmentTransaction fragmentTransaction) {
        fragmentTransaction.replace(R.id.container, new NewEmployeesFragment()).commit();
    }

    @Override
    public void startSettingsFragment(@NonNull FragmentTransaction fragmentTransaction) {
        fragmentTransaction.replace(R.id.container, new SettingsFragment()).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentManager fragmentManager =  getSupportFragmentManager();
        presenter.onNavigationItemSelected(item, fragmentManager);
        return true;
    }


    private  void initCompanies(final FragmentTransaction fragmentTransaction) {
        App.getApi().getCompanies(App.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        new Delete().from(DataCompanies.class).execute();
                        new Delete().from(InnerColor.class).execute();
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String jsonString = jsonObject.getString("data");
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                        Gson gson = builder.create();
                        final CompaniesManager companiesManager = gson.fromJson(jsonString, CompaniesManager.class);
                        if (companiesManager.getData().size()!=0){
                            for (DataCompanies dataCompanies : companiesManager.getData()){
                                dataCompanies.save();
                                dataCompanies.getInnerColor().save();
                            }
                            if (App.getCompany()==null){
                                App.setCompany(companiesManager.getData().get(0).getIdCompanies().toString());
                            }
                            if (App.getCompany2()==null){
                                App.setCompany2(companiesManager.getData().get(0).getIdCompanies().toString());
                            }
                        }
                        fragmentTransaction.replace(R.id.container, new EmployeesFragment()).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    fragmentTransaction.replace(R.id.container, new EmployeesFragment()).commit();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                fragmentTransaction.replace(R.id.container, new EmployeesFragment()).commit();
            }
        });
    }

    private void initIconSizeBottomNavigationMenuView(){
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }
    }
}
