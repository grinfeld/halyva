package com.bpworld.admin.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "#SettingsFragment";
    private Activity context;
    private ActionBar actionBar;

    @BindView(R.id.input_old_password)
    EditText etOldPassword;
    @BindView(R.id.input_login)
    EditText etLogin;
    @BindView(R.id.input_new_password)
    EditText etNewPassword;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        context = getActivity();
        if (context != null) {
            ActionBar actionBar = ((AppCompatActivity)context).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                View view = getLayoutInflater().inflate(R.layout.custom_action_bar_settings, null);
                ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER);
                actionBar.setCustomView(view, params);
                TextView btn_action_save = view.findViewById(R.id.btn_action_save);
                btn_action_save.setOnClickListener(this);
                TextView btn_action_exit = view.findViewById(R.id.btn_action_exit);
                btn_action_exit.setOnClickListener(this);
            }
            etLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_login), null);
            etOldPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_password), null);
            etNewPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_password), null);
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_action_exit:
                new Delete().from(DataCompanies.class).execute();
                new Delete().from(InnerColor.class).execute();
                App.setToken(null);
                App.setCompany2(null);
                App.setCompany(null);
                startActivity(new Intent(context, LoginActivity.class));
                context.finish();
                context. overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                break;
            case R.id.btn_action_save:
                App.getApi().getManager(App.getToken()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.isSuccessful()) {
                                String strResponse = response.body().string();
                                JSONObject jsonObject = new JSONObject(strResponse);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String id = jsonObject1.getString("id");
                                App.getApi().updateManager(App.getToken(), Integer.parseInt(id),etOldPassword.getText().toString(),etLogin.getText().toString(),etNewPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        try {
                                            if (response.isSuccessful()){
                                                Log.d(TAG, "isSuccessful "+response.body().string());
                                            }else {
                                                String strResponse = response.errorBody().string();
                                                JSONObject jsonObject = new JSONObject(strResponse);
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("errors");
                                                String description = jsonObject1.getString("description");
                                                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });
                                Log.d(TAG, id);
                            }else {
                                Log.d(TAG, response.errorBody().string());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                break;
        }
    }
}
