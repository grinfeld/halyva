package com.bpworld.admin.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.bpworld.admin.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by GrinfeldRA
 */

public class CustomDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    @BindView(R.id.action_camera)
    TextView action_camera;
    @BindView(R.id.action_select_gallery)
    TextView action_select_gallery;
    @BindView(R.id.action_cancel)
    TextView action_cancel;

    public interface IOnActionClickDialog {
        void onActionClickGallery();
        void onActionClickCamera();
    }

    IOnActionClickDialog iOnActionClickDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_dialog, container, false);
        ButterKnife.bind(this, rootView);
        iOnActionClickDialog = (IOnActionClickDialog) getFragmentManager().findFragmentById(R.id.container);
        action_camera.setOnClickListener(this);
        action_select_gallery.setOnClickListener(this);
        action_cancel.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_camera:
                iOnActionClickDialog.onActionClickCamera();
                dismiss();
                break;
            case R.id.action_select_gallery:
                iOnActionClickDialog.onActionClickGallery();
                dismiss();
                break;
            case R.id.action_cancel:
                dismiss();
                break;
        }
    }
}
