package com.bpworld.admin.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.bpworld.admin.App;
import com.bpworld.admin.R;
import com.bpworld.admin.model.DataCompanies;
import com.bpworld.admin.model.InnerColor;
import com.bpworld.admin.util.OperationUtil;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by GrinfeldRA
 */

public class NewEmployeesFragment extends Fragment implements View.OnClickListener, CustomDialogFragment.IOnActionClickDialog {


    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int SELECT_PHOTO = 111;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 101;

    private static final String TAG = "#NewEmployeesFragment";
    private Activity context;
    private ActionBar actionBar;
    @BindView(R.id.action_point_view)
    View action_point_view;
    @BindView(R.id.action_point)
    ImageView action_point;
    @BindView(R.id.input_name)
    EditText etName;
    @BindView(R.id.input_surname)
    EditText etSurname;
    @BindView(R.id.input_telephone)
    EditText etTelephone;
    @BindView(R.id.input_login)
    EditText etLogin;
    @BindView(R.id.txt_title)
    TextView point_name;
    @BindView(R.id.circle)
    View point_circle;
    @BindView(R.id.profile_image)
    CircleImageView circleImageView;
    private DataCompanies currentDataCompanies;
    private MultipartBody.Part body;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_employees, container, false);
        context = getActivity();
        ButterKnife.bind(this, rootView);
        action_point.setVisibility(View.VISIBLE);
        action_point_view.setOnClickListener(this);
        if (context != null) {
            ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
                TextView btn_action = viewActionBar.findViewById(R.id.btn_action);
                TextView txt_title = viewActionBar.findViewById(R.id.txt_title);
                btn_action.setVisibility(View.VISIBLE);
                btn_action.setOnClickListener(this);
                btn_action.setText(getString(R.string.str_create));
                txt_title.setText(getString(R.string.str_new_employees));
                actionBar.setCustomView(viewActionBar);
                actionBar.setElevation(0);
                actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            }

            etLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_login), null);
            etName.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_login), null);
            etSurname.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_login), null);
            etTelephone.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_phone), null);
            circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickActionAddAvatar();
                }
            });
            List<InnerColor> innerColors = new Select().from(InnerColor.class).execute();
            List<DataCompanies> dataCompanies = new Select().from(DataCompanies.class).execute();
            int currentPosition = 0;
            for (DataCompanies companies : dataCompanies) {
                if (App.getCompany2().equals(companies.getIdCompanies().toString())) {
                    currentDataCompanies = companies;
                    break;
                }
                currentPosition += 1;
            }
            if (currentDataCompanies != null) {
                OperationUtil.setBackgroundDrawable((StateListDrawable) point_circle.getBackground(), innerColors.get(currentPosition).getHex());
                point_name.setText(currentDataCompanies.getInnerName());
            }
        }
        return rootView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_action:
                if (!etLogin.getText().toString().isEmpty() && !etName.getText().toString().isEmpty()
                        && !etSurname.getText().toString().isEmpty() && !etTelephone.getText().toString().isEmpty()) {
                    LayoutInflater factory = LayoutInflater.from(getActivity());
                    final View deleteDialogView = factory.inflate(R.layout.custom_dialog, null);
                    final AlertDialog createUserDialog = new AlertDialog.Builder(getActivity()).create();
                    createUserDialog.setView(deleteDialogView);
                    TextView textView = deleteDialogView.findViewById(R.id.txt_dia);
                    Button yes = deleteDialogView.findViewById(R.id.btn_yes);
                    Button no = deleteDialogView.findViewById(R.id.btn_no);
                    textView.setText("Вы действительно хотите создать пользователя?");
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_green, yes);
                    setBackgroundDrawable(getActivity(), R.drawable.btn_selector_red, no);
                    yes.setText("Создать");
                    no.setText("Отмена");
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //your business logic
                            createUserDialog.dismiss();
                        }
                    });
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RequestBody login = RequestBody.create(MediaType.parse("text/plain"), etLogin.getText().toString());
                            RequestBody fio = RequestBody.create(MediaType.parse("text/plain"), etName.getText().toString() + " " + etSurname.getText().toString());
                            RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), etTelephone.getText().toString());
                            RequestBody company_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(currentDataCompanies.getIdCompanies()));
                            App.getApi().createCashier(App.getToken(), login, fio, phone, company_id, body).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    try {
                                        if (response.isSuccessful()) {
                                            Log.d(TAG, response.body().string());
                                            Toast.makeText(getActivity(), "ОК", Toast.LENGTH_SHORT).show();
                                            createUserDialog.dismiss();
                                        } else {
                                            String strResponse = response.errorBody().string();
                                            Log.d(TAG, strResponse);
                                            JSONObject jsonObject = new JSONObject(strResponse);
                                            JSONObject data = jsonObject.getJSONObject("errors");
                                            String description = data.getString("description");
                                            Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                                            createUserDialog.dismiss();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                    createUserDialog.dismiss();
                                }
                            });
                        }
                    });
                    createUserDialog.show();
                } else {
                    Toast.makeText(getActivity(), "Заполните все поля", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_point_view:
                PointFragment pointFragment = new PointFragment();
                Bundle bundle = new Bundle();
                bundle.putString("KEY", "NEW");
                pointFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container, pointFragment).addToBackStack(null).commit();
                break;
        }
    }


    public void onClickActionAddAvatar() {
        CustomDialogFragment mBottomSheetDialog = new CustomDialogFragment();
        mBottomSheetDialog.show(getFragmentManager(), "add_photo_dialog_fragment");
    }

    @Override
    public void onActionClickGallery() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                } else {
                    ActivityCompat.requestPermissions(context,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                }
            } else {
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        } else {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            Log.d("TAG", "onActionClickGallery");
        }

    }

    @Override
    public void onActionClickCamera() {
        Log.d("TAG", String.valueOf(Build.VERSION.SDK_INT));
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new
                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(context, "camera permission denied", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            } else {
                Toast.makeText(context, "read external storage permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            circleImageView.setImageBitmap(photo);
            File file = createAvatarFile(photo);
            if (file != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

            }
        } else if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
            circleImageView.setImageBitmap(bitmap);
            File file = createAvatarFile(bitmap);
            if (file != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

            }
            cursor.close();
        }
    }

    private File createAvatarFile(Bitmap bitmap) {
        ///create a file to write bitmap data
        try {
            File f = new File(context.getCacheDir(), "avatar");
            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setBackgroundDrawable(Context context, int id, View view) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, id));
        } else {
            view.setBackground(ContextCompat.getDrawable(context, id));
        }
    }

}
