package com.bpworld.admin.interfaces;

import com.bpworld.admin.model.OauthToken;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by GrinfeldRA
 */

public interface Api {

    @FormUrlEncoded
    @POST("oauth/login")
    Call<OauthToken> getOauthToken(@Field("client_id") String client_id,
                                   @Field("client_secret") String client_secret,
                                   @Field("email") String email,
                                   @Field("password") String password,
                                   @Field("provider") String provider);

    @Multipart
    @POST("api/cashiers")
    Call<ResponseBody> createCashier(@Header("Authorization")String token,
                                     @Part("login")RequestBody login,
                                     @Part("fio") RequestBody fio,
                                     @Part("phone_number")RequestBody phone_number,
                                     @Part("company_id")RequestBody company_id,
                                     @Part() MultipartBody.Part file);


    @GET("api/manager/companies")
    Call<ResponseBody> getCompanies(@Header("Authorization")String token);


    @GET("api/cashiers")
    Call<ResponseBody> getCashiers(@Header("Authorization")String token,
                                   @QueryMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/cashiers/{id}/status")
    Call<ResponseBody> cashierStatus(@Header("Authorization")String token,
                                     @Path("id")int id,
                                     @Field("is active")int is_active);


    @GET("api/cashiers/{id}/operations")
    Call<ResponseBody> getOperationsCashier(@Header("Authorization")String token,
                                            @Path("id")int id);

    @GET("api/operations/{id}")
    Call<ResponseBody> getOperations(@Header("Authorization")String token,
                                     @Path("id")int id);

    @POST("api/cashiers/{id}/send_password")
    Call<ResponseBody> sendPassword(@Header("Authorization")String token,
                                    @Path("id")int id);

    @DELETE("api/cashiers/{id}")
    Call<ResponseBody> deleteCashier(@Header("Authorization")String token,
                                     @Path("id")int id);

    @GET("api/manager/me")
    Call<ResponseBody> getManager(@Header("Authorization")String token);

    @FormUrlEncoded
    @PUT("api/managers/{id}")
    Call<ResponseBody> updateManager(@Header("Authorization")String token,
                                     @Path("id")int id,
                                     @Field("old_password") String old_password,
                                     @Field("login ")String login,
                                     @Field("password")String password);

}
