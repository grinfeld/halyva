package com.bpworld.admin.interfaces.contract;

/**
 * Created by GrinfeldRA
 */

public interface CustomDialogContract {

    interface view{
        void onStart();
        void onStartSave();
    }

    interface presenter{
        void onActionOK();
    }

}
