package com.bpworld.admin.interfaces;

import android.support.v7.widget.RecyclerView;

/**
 * Created by GrinfeldRA
 */

public interface OnStartDragListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);

}
