package com.bpworld.admin.interfaces.contract;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

/**
 * Created by GrinfeldRA.
 */

public interface MainActivityContract {

    interface view{
        void startEmployeesFragment(@NonNull FragmentTransaction fragmentTransaction);
        void startNewEmployeesFragment(@NonNull FragmentTransaction fragmentTransaction);
        void startSettingsFragment(@NonNull FragmentTransaction fragmentTransaction);
    }

    interface presenter{
        void onNavigationItemSelected(@NonNull MenuItem menuItem, FragmentManager fragmentManager);
    }

}
