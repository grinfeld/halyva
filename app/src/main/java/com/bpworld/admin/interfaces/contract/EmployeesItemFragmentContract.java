package com.bpworld.admin.interfaces.contract;

/**
 * Created by GrinfeldRA
 */

public interface EmployeesItemFragmentContract {

    interface view{
        void onStartSendPassword();
        void onStartDeleteEmployees();
    }

    interface presenter {
        void onActionSendPassword();
        void onActionDeleteEmployees();
    }
}
