package com.bpworld.admin.interfaces.contract;

/**
 * Created by GrinfeldRA.
 */

public interface EmployeesFragmentContract {

    interface view{
        void onStartEdit();
        void onStartSave();
        void onStartPoint();
        void onStartEmployeesItem(int position);
    }

    interface presenter{
        void onActionEdit();
        void onActionSave();
        void onActionPoint();
        void onActionRecyclerViewItem(int position);
    }
}
