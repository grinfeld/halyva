package com.bpworld.admin.interfaces;

/**
 * Created by GrinfeldRA
 */

public interface ItemTouchHelperCallback {

    boolean onItemMove(int fromPosition, int toPosition);

}
