package com.bpworld.admin;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by GrinfeldRA
 */

public class ConnectivityInterceptor implements Interceptor {

    private Context mContext;
    Handler handler;
    public ConnectivityInterceptor(Context context) {
        mContext = context;
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!isOnline(mContext)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext, "Нет интернет соединения", Toast.LENGTH_SHORT).show();
                }
            });
        }

        Request.Builder builder = chain.request().newBuilder();
        builder.header("Accept", "application/json");
        return chain.proceed(builder.build());
    }

    private  boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

}