package com.bpworld.admin.presenter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.bpworld.admin.R;
import com.bpworld.admin.interfaces.contract.MainActivityContract;
/**
 * Created by GrinfeldRA.
 */

public class MainActivityPresenter implements MainActivityContract.presenter{


    private final MainActivityContract.view view;

    public MainActivityPresenter(MainActivityContract.view view) {
        this.view = view;
    }


    @Override
    public void onNavigationItemSelected(@NonNull MenuItem menuItem, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = null;
        if (fragmentManager != null) {
            fragmentTransaction = fragmentManager.beginTransaction();
        }
        switch (menuItem.getItemId()) {
            case R.id.action_employees:
                if (fragmentTransaction != null) {
                    view.startEmployeesFragment(fragmentTransaction);
                }
                break;
            case R.id.action_password_requests:
                if (fragmentTransaction != null) {
                    view.startNewEmployeesFragment(fragmentTransaction);
                }
                break;
            case R.id.action_settings:
                if (fragmentTransaction != null) {
                    view.startSettingsFragment(fragmentTransaction);
                }
                break;
        }
    }

}
