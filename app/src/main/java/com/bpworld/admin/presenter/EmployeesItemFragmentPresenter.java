package com.bpworld.admin.presenter;

import com.bpworld.admin.interfaces.contract.EmployeesItemFragmentContract;
import com.bpworld.admin.view.EmployeesItemFragment;

/**
 * Created by GrinfeldRA
 */

public class EmployeesItemFragmentPresenter implements EmployeesItemFragmentContract.presenter {

    EmployeesItemFragmentContract.view view;

    public EmployeesItemFragmentPresenter(EmployeesItemFragmentContract.view view) {
        this.view = view;
    }

    @Override
    public void onActionSendPassword() {
        view.onStartSendPassword();
    }

    @Override
    public void onActionDeleteEmployees() {
        view.onStartDeleteEmployees();
    }
}
