package com.bpworld.admin.presenter;

import com.bpworld.admin.interfaces.contract.EmployeesFragmentContract;

/**
 * Created by GrinfeldRA.
 */

public class EmployeesFragmentPresenter implements EmployeesFragmentContract.presenter{


    private EmployeesFragmentContract.view view;

    public EmployeesFragmentPresenter(EmployeesFragmentContract.view view) {
        this.view = view;
    }

    @Override
    public void onActionEdit() {
        view.onStartEdit();
    }

    @Override
    public void onActionSave() {
        view.onStartSave();
    }

    @Override
    public void onActionPoint() {
        view.onStartPoint();
    }

    @Override
    public void onActionRecyclerViewItem(int position) {
        view.onStartEmployeesItem(position);
    }

}
